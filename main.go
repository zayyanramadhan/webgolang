package main

import (
	"encoding/json"
	"gouser/service"
	"net/http"
)

var PORT = ":8183"

func main() {
	http.HandleFunc("/", greet)
	http.HandleFunc("/register", input)
	http.HandleFunc("/user", getdata)
	http.ListenAndServe(PORT, nil)
}

func greet(w http.ResponseWriter, r *http.Request) {
	msg := "hello world"
	w.Write([]byte(msg))
}

func input(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		name := r.FormValue("name")
		res := register(name)
		json.NewEncoder(w).Encode(res)
	}

}

func getdata(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		res := getdatauser()
		json.NewEncoder(w).Encode(res)
	}
}

var data []*service.User
var userData = service.NewUserService(data)

func register(name string) string {
	test := userData.SimpanUser(&service.User{Name: name})

	return test
}

func getdatauser() []*service.User {
	testget := userData.Tampilkandata()
	return testget
}
